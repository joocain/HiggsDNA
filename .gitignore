# Editors
.vscode/
.idea/

# Vagrant
.vagrant/

# Mac/OSX
.DS_Store

# Windows
Thumbs.db

# Source for the following rules: https://raw.githubusercontent.com/github/gitignore/master/Python.gitignore
# Byte-compiled / optimized / DLL files
__pycache__/
*.py[cod]
*$py.class

# C extensions
*.so

# Distribution / packaging
.Python
build/
develop-eggs/
dist/
downloads/
eggs/
.eggs/
lib/
lib64/
parts/
sdist/
var/
wheels/
*.egg-info/
.installed.cfg
*.egg
MANIFEST

# PyInstaller
#  Usually these files are written by a python script from a template
#  before PyInstaller builds the exe, so as to inject date/other infos into it.
*.manifest
*.spec

# Installer logs
pip-log.txt
pip-delete-this-directory.txt

# Unit test / coverage reports
htmlcov/
.tox/
.nox/
.coverage
.coverage.*
.cache
nosetests.xml
coverage.xml
*.cover
.hypothesis/
.pytest_cache/

# Translations
*.mo
*.pot

# Django stuff:
*.log
local_settings.py
db.sqlite3

# Flask stuff:
instance/
.webassets-cache

# Scrapy stuff:
.scrapy

# Sphinx documentation
docs/_build/

# PyBuilder
target/

# Jupyter Notebook
.ipynb_checkpoints

# IPython
profile_default/
ipython_config.py

# pyenv
.python-version

# celery beat schedule file
celerybeat-schedule

# SageMath parsed files
*.sage.py

# Environments
.env
.venv
env/
venv/
ENV/
env.bak/
venv.bak/

# Spyder project settings
.spyderproject
.spyproject

# Rope project settings
.ropeproject

# mkdocs documentation
/site

# mypy
.mypy_cache/
.dmypy.json
dmypy.json

# tagger output files
examples/output/
scripts/output/
tests/output/
tests/log

# other stuff
.higgs_dna_vanilla_lxplus
dask-report.html
*.cc

# sshfs on mac
._*
Diphoton_CDFs.pkl.gx

# Sample directory (input txts and output jsons)
higgs_dna/samples/*.txt
higgs_dna/samples/*.json

### The following ingredients can be pulled with scripts/pull_files.py and are therefore not added to the index

## golden JSON
higgs_dna/metaconditions/CAF/

## JSONs for systematics
# Weight-based Egamma objects
higgs_dna/systematics/JSONs/TriggerSF/
higgs_dna/systematics/JSONs/Preselection/
higgs_dna/systematics/JSONs/ElectronVetoSF/
higgs_dna/systematics/JSONs/SF_photon_ID/
higgs_dna/systematics/JSONs/LooseMvaSF/
# Weight-based jets
higgs_dna/systematics/JSONs/cTagSF/
higgs_dna/systematics/JSONs/bTagSF/
# Weight-based event level
higgs_dna/systematics/JSONs/pileup
# Variable-based Egamma (potentially custom for Hgg)
higgs_dna/systematics/JSONs/scaleAndSmearing/
higgs_dna/systematics/JSONs/FNUF/
higgs_dna/systematics/JSONs/Material
higgs_dna/systematics/JSONs/ShowerShape/
# Variable-based central (e.g. JME)
higgs_dna/systematics/JSONs/POG/
# JEC and JER (not sure why it is named data to be honest)
higgs_dna/systematics/data/

# CDFs for decorrelation
higgs_dna/tools/Diphoton_CDFs.pkl.gz
higgs_dna/tools/Smeared_Diphoton_CDFs.pkl.gz
higgs_dna/tools/decorrelation_CDFs/

# Flow models
higgs_dna/tools/flows/

# analysis specific stuff
higgs_dna/tools/lowmass_diphoton_mva/
